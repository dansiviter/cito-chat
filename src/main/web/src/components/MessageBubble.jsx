import React from 'react';

import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import Person from 'material-ui/svg-icons/social/person';
import Paper from 'material-ui/Paper';

/*
 * @class MessageBubble
 * @extends React.Component
 */
class MessageBubble extends React.Component {
    constructor(props) {
        super(props);
    }

    /*
     * @method render
     * @returns {JSX}
     */
    render() {
        return <div style={{ display: 'flex', flexDirection: 'row', margin: '1em' }}>
            <Avatar icon={<Person />} /> 
            <Paper style={{ width: '100%', marginLeft: '1em', padding: '0.5em' }} zDepth={ 1 } >
                Hello World!
            </Paper>
        </div>;
    }
}

export default MessageBubble;