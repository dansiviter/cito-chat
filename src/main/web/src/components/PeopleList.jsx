import React from 'react';

import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import Person from 'material-ui/svg-icons/social/person';

import Stomp from '../Stomp';

/*
 * @class PeopleList
 * @extends React.Component
 */
class PeopleList extends React.Component {
    constructor(props) {
        super(props);

        this.handlePeopleList = this.handlePeopleList.bind(this);
    }

    
    componentDidMount() {
        Stomp.subscribe('topic/users', (body) => {
            console.log(body);
        });
    }
    /*
     * 
     */
    handlePeopleList() {
        const PeopleList = this.refs.PeopleListField.getValue();
        Stomp.connect(PeopleList);
    }

    /*
     * @method render
     * @returns {JSX}
     */
    render() {
        return <List style={{ backgroundColor: 'lightgrey', height: '100%' }}>
            <Subheader>People</Subheader>
            <ListItem
                primaryText="User 1"
                leftAvatar={<Avatar icon={<Person />} />} />
        </List>;
    }
}

export default PeopleList;