import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from "react-tap-event-plugin";

import App from './App.jsx';

injectTapEventPlugin();


render(<App style={{ height: '100%' }}/>, document.querySelector("#app"));
