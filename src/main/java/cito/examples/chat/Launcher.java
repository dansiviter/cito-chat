/*

 * Copyright 2016-2017 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.examples.chat;

import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.management.ManagementFraction;

/**
 * @author Daniel Siviter
 * @since v1.0 [23 Apr 2017]
 */
public class Launcher {
	public static void main(String[] args) throws Exception {
		final Swarm swarm = new Swarm(args);
		try {
			swarm.fraction(new ManagementFraction()
					.securityRealm("ApplicationRealm", (realm) -> {
						realm.inMemoryAuthentication( (authn)-> {
							authn.add("$Y$T3M", "Pa$$w0rd", true);
							authn.add("eric", "password", true);
							authn.add("kenny", "password", true);
							authn.add("kyle", "password", true);
							authn.add("chef", "password", true);
							authn.add("bebe", "password", true);
							authn.add("token", "password", true);
							authn.add("sharon", "password", true);
							authn.add("liane", "password", true);
							authn.add("sheila", "password", true);
						});
					})).start().deploy();

		} catch (final VirtualMachineError vme) {
			// Don't even try to swarm.stop() in case of OOM etc.
			vme.printStackTrace();
			System.exit(1);
		} catch (final Throwable t) {
			t.printStackTrace();
			swarm.stop();
		}
	}
}
