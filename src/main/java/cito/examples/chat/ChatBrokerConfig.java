/*
 * Copyright 2016-2017 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.examples.chat;

import static java.util.Arrays.asList;

import java.util.Collections;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Specializes;

import org.apache.activemq.artemis.api.core.BroadcastEndpointFactory;
import org.apache.activemq.artemis.api.core.BroadcastGroupConfiguration;
import org.apache.activemq.artemis.api.core.DiscoveryGroupConfiguration;
import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.core.UDPBroadcastEndpointFactory;
import org.apache.activemq.artemis.core.config.ClusterConnectionConfiguration;
import org.apache.activemq.artemis.core.config.Configuration;
import org.apache.activemq.artemis.core.config.HAPolicyConfiguration;
import org.apache.activemq.artemis.core.config.ScaleDownConfiguration;
import org.apache.activemq.artemis.core.config.ha.LiveOnlyPolicyConfiguration;

import cito.broker.artemis.BrokerConfig;

/**
 * Distributed embedded broker for chat.
 * 
 * @author Daniel Siviter
 * @since v1.0 [16 Apr 2017]
 */
@Specializes @ApplicationScoped
public class ChatBrokerConfig extends BrokerConfig {
	private static final String PREFIX = "chat-";
	private static final String BROADCAST_GROUP = PREFIX.concat("broadcast-group");
	private static final String DISCOVERY_GROUP = PREFIX.concat("discovery-group");

	@Override
	public Configuration getConfiguration() {
		final BroadcastEndpointFactory endpointFactory = new UDPBroadcastEndpointFactory()
				.setGroupPort(9876)
				.setGroupAddress("231.7.7.7");

		return super.getConfiguration()
				.addAcceptorConfiguration(new TransportConfiguration(REMOTE_ACCEPTOR, Collections.<String,Object>emptyMap(), "netty"))
				.addConnectorConfiguration("netty", new TransportConfiguration(REMOTE_CONNECTOR))
				.addBroadcastGroupConfiguration(broadcastGroupConfig(endpointFactory))
				.addDiscoveryGroupConfiguration(DISCOVERY_GROUP, discoveryGroupConfig(endpointFactory))
				.addClusterConfiguration(clusterConnectionConfig())
				.setHAPolicyConfiguration(haPolicyConfig());
	}



	/**
	 * 
	 * @param endpointFactory
	 * @return
	 */
	private static BroadcastGroupConfiguration broadcastGroupConfig(BroadcastEndpointFactory endpointFactory) {
		return new BroadcastGroupConfiguration()
				.setName(BROADCAST_GROUP)
				.setEndpointFactory(endpointFactory)
				.setConnectorInfos(asList("netty"));
	}

	/**
	 * 
	 * @param endpointFactory 
	 * @return
	 */
	private static DiscoveryGroupConfiguration discoveryGroupConfig(BroadcastEndpointFactory endpointFactory) {
		return new DiscoveryGroupConfiguration()
				.setName(DISCOVERY_GROUP)
				.setBroadcastEndpointFactory(endpointFactory);
	}

	/**
	 * 
	 * @return
	 */
	private static ClusterConnectionConfiguration clusterConnectionConfig() {
		return new ClusterConnectionConfiguration()
				.setName("chat-cluster")
				.setConnectorName("netty")
				.setDiscoveryGroupName(DISCOVERY_GROUP);
	}

	/**
	 * 
	 * @return
	 */
	private static HAPolicyConfiguration haPolicyConfig() {
		return new LiveOnlyPolicyConfiguration(new ScaleDownConfiguration().setDiscoveryGroup(DISCOVERY_GROUP));
	}
}
